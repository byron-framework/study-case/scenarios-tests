# Test Scenarios

This repository have the code needed to run the test scenarios for Byron. This
document have the steps to run the tests

## Install Byron

Firstly install the Byron CLI:

```bash
npm install --global @byronframework/cli
# or
yarn global add @byronframework/cli
```

Make sure you have `docker` and `docker-compose` installed.

## Cloning the components

Clone the two components that we use for the testing and this repository to run
the tests

```bash
git clone https://gitlab.com/byron-framework/study-case/auth
git clone https://gitlab.com/byron-framework/study-case/grouping
git clone https://gitlab.com/byron-framework/study-case/scenario-tests
```

## Running the components

Before running the tests, we need to run broker first.

```bash
byron infrastructure up -N StudyCase
```

Then build and run the components.

```bash
byron component build Auth && byron component up Auth
byron component build Grouping && byron component up Grouping
```

## Running the tests

To run all scenarios just up the container.

```bash
docker-compose up --build
```

To run a specific scenario, use one of the commands.

```bash
docker-compose run tester yarn first
docker-compose run tester yarn second
docker-compose run tester yarn third
```

**NOTE**: To run the tests a second time, after running the third scenario, you
have to down all the components, the broker and delete the broker volume.

```bash
byron component down Auth
byron component down Grouping
byron infrastructure down -N StudyCase
docker volume rm broker_data_lake
```