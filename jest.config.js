module.exports = {
  "testEnvironment": "node",
  "testRegex": ".+\.(spec|test).ts",
  "roots": [
    "<rootDir>/src/",
    "<rootDir>/tests/"
  ],
  "modulePaths": [
    "."
  ],
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  "testPathIgnorePatterns": [
    "/node_modules/",
    "./lib"
  ],
  "collectCoverageFrom": [
    "**/*.{js,jsx,ts,tsx}",
    "!<rootDir>/src/index.ts",
    "!<rootDir>/src/generators/Generator.ts",
    "!<rootDir>/src/utils/compExec.ts"
  ],
  "coverageReporters": [
    "lcov",
    "text",
    "json-summary"
  ],
  "coverageThreshold": {
    "global": {
      "statements": 80,
      "branches": 80,
      "functions": 80,
      "lines": 80,
    },
  },
}
