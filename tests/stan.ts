import {
  connect,
  Stan,
  SubscriptionOptions,
} from 'node-nats-streaming';

import uuid from 'uuid/v4';

export interface Message {
  getData(): string;
}

export const stan: Stan = connect('test-cluster', uuid(), {
  url: 'nats://broker:4222'
});

export const replyAll: SubscriptionOptions = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();
