import assert from 'assert';
import axios from 'axios';

import { Message, replyAll, stan } from './stan';
import { authURL, groupingURL } from './URLs';
import { sleep } from './utils';

const users: any[] = [];
const schools: any[] = [];

function listenToNewUser(): void {
  stan
    .subscribe('new.user', replyAll)
    .on('message', (msg: Message): void => {
      const data: any = JSON.parse(msg.getData());
      users.push(data);
    });
}

function listenToNewSchool(): void {
  stan
    .subscribe('new.school', replyAll)
    .on('message', (msg: Message): void => {
      const data: any = JSON.parse(msg.getData());
      schools.push(data);
    });
}

function listenToNewUserRole(): void {
  stan
    .subscribe('new.user.role', replyAll)
    .on('message', (msg: Message) => {
      const data: any = JSON.parse(msg.getData());
      const index: number = users
        .findIndex((user: any): boolean => user._id === data.id);
      users[index].role = data.role;
    });
}

function checkUserCreation(id: string): void {
  const containsId: boolean = users
    .some((user: any): boolean => user._id === id );

  try {
    assert.ok(containsId, `User with id: ${id} not found`);
    console.log(`User with id: ${id} handled`);
  } catch (e) {
    console.log(e);
  }
}

function checkSchoolCreation(id: string): void {
  const containsId: boolean = schools
    .some((school: any): boolean => school._id === id );

  try {
    assert.ok(containsId, `School with id: ${id} not found`);
    console.log(`School with id: ${id} handled`);
  } catch (e) {
    console.log(e);
  }
}

function checkUserRoleUpdate(id: string): void {
  const containsId: boolean = users
    .some((user: any): boolean => user._id === id && user.role === 'coord' );

  try {
    assert.ok(containsId, `User with id: ${id} not found or doesn't have 'coord' role`);
    console.log(`User with id: ${id} was found with 'coord' role`);
  } catch (e) {
    console.log(e);
  }
}

async function createUser(): Promise<string> {
  const createCommand: string = `
    mutation m {
      createUser(data: {
        name: "John Doe"
        email: "${new Date().getTime()}@mail.com"
        password: "123456"
        passwordConfirmation: "123456"
      }) {
      _id
      }
    }
  `;

  const res: any = await axios({
    method: 'post',
    url: authURL,
    data: { query: createCommand },
  });

  const _id: string = res.data.data.createUser._id;

  try {
    assert.ok(_id, 'User creation went wrong');
    console.log(`User created correctly with id: ${_id}`);
  } catch (e) {
    console.log(e);
  }

  return _id;
}

async function createSchool(id: string): Promise<string> {
  const createCommand: string = `
    mutation m {
      createSchool (
        data: {
          name: "${new Date().getTime()}"
          address: "Rua do Matão, 1010"
        }
        userId: "${id}"
      ) {
        _id
      }
    }
  `;

  const res: any = await axios({
    method: 'post',
    url: groupingURL,
    data: { query: createCommand },
  });

  const _id: any = res.data.data.createSchool._id;

  try {
    assert.ok(_id, 'School creation went wrong');
    console.log(`School created correctly with id: ${_id}`);
  } catch (e) {
    console.log(e);
  }

  return _id;
}

async function readSchool(id: string): Promise<void> {
  const readCommand: string = `
    query q {
      getSchools {
        _id
      }
    }
  `;

  const { data: { data } }: any = await axios({
    method: 'post',
    url: groupingURL,
    data: { query: readCommand },
  });

  try {
    const containsId: boolean = data.getSchools
      .some((school: any): boolean => school._id === id );

    assert.ok(containsId, 'School is not present');
    console.log(`School read correctly with id: ${id}`);
  } catch (e) {
    console.log(e);
  }
}

export async function test(): Promise<void> {
  console.log('###   2nd Scenario   ###\n');

  const userId: string = await createUser();
  listenToNewUser();
  await sleep(1000);

  checkUserCreation(userId);

  const schoolId: string = await createSchool(userId);
  listenToNewSchool();
  listenToNewUserRole();
  await sleep(1000);

  checkSchoolCreation(schoolId);
  checkUserRoleUpdate(userId);

  readSchool(schoolId);

  stan.close();

  console.log('\n########################\n');
}

test();
