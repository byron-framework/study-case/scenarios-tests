import assert from 'assert';
import axios from 'axios';

import { Message, replyAll, stan } from './stan';
import { authURL } from './URLs';
import { sleep } from './utils';

const users: any[] = [];

function listenToNewUser(): void {
  stan
    .subscribe('new.user', replyAll)
    .on('message', (msg: Message): void => {
      const data: any = JSON.parse(msg.getData());
      users.push(data);
    });
}

function checkUserCreation(id: string): void {
  const containsId: boolean = users
    .some((user: any) => user._id === id );

  try {
    assert.ok(containsId, `User with id: ${id} not found`);
    console.log(`User with id: ${id} handled`);
  } catch (e) {
    console.log(e);
  }
}

async function createUser(): Promise<string> {
  const createCommand: string = `
    mutation m {
      createUser(data: {
        name: "John Doe"
        email: "${new Date().getTime()}@mail.com"
        password: "123456"
        passwordConfirmation: "123456"
      }) {
      _id
      }
    }
  `;

  const res: any = await axios({
    method: 'post',
    url: authURL,
    data: { query: createCommand },
  });

  const _id: string = res.data.data.createUser._id;

  try {
    assert.ok(_id, 'User creation went wrong');
    console.log(`User created correctly with id: ${_id}`);
  } catch (e) {
    console.log(e);
  }

  return _id;
}

async function readUser(id: string): Promise<void> {
  const readCommand: string = `
    query q {
      getUser(id: '${id}') {
        _id
      }
    }
  `;

  const { data: { data } }: any = await axios({
    method: 'post',
    url: authURL,
    data: { query: readCommand },
  });

  try {
    assert.equal(data.getUser._id, id, 'User read incorrectly');
    console.log(`User read correctly with id: ${id}`);
  } catch (e) {
    console.log(e);
  }
}

export async function test(): Promise<void> {
  console.log('###   1st Scenario   ###\n');

  const id: string = await createUser();
  listenToNewUser();

  await sleep(1000);

  checkUserCreation(id);
  await readUser(id);

  stan.close();

  console.log('\n########################\n');
}

test();
