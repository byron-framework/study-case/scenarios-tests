export function sleep(ms: number): Promise<any> {
  return new Promise((resolve: any): void => {
    setTimeout(resolve, ms);
  });
}
